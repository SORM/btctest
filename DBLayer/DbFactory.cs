﻿using DBLayer.Contexts;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DBLayer
{
    public class DbFactory : IDesignTimeDbContextFactory<BtcContext>
    {      
        public BtcContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var db = new BtcContext(config);
            return db;
        }
    }
}