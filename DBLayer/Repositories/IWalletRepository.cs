﻿using System.Threading.Tasks;
using DBLayer.Models;

namespace DBLayer.Repositories
{
    public interface IWalletRepository
    {
        Task UpdateAddressesAsync(AddressInfo[] infos);
        Task<int> CountAsync();
        Task UpdateAsync(AddressInfo addressInfo);
    }
}