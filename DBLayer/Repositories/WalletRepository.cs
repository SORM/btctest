﻿using System;
using System.Threading.Tasks;
using DBLayer.Contexts;
using DBLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DBLayer.Repositories
{
    public class WalletRepository : IWalletRepository
    {
        private BtcContext _btcContext;

        public WalletRepository(BtcContext context)
        {
            _btcContext = context;
        }

        public async Task UpdateAddressesAsync(AddressInfo[] infos)
        {
            foreach (var addressInfo in infos)
            {
                await UpdateAsync(addressInfo);
            }
        }

        public Task<int> CountAsync()
        {
            return _btcContext.Addresses.CountAsync();
        }

        public async Task UpdateAsync(AddressInfo addressInfo)
        {
            if (addressInfo == null)
                throw new ArgumentNullException(nameof(addressInfo));

            if(string.IsNullOrEmpty(addressInfo.Id))
                throw new ArgumentException("Address id is null");

            bool updateFailed;
            do
            {
                updateFailed = false;
                var current = await _btcContext.Addresses.FindAsync(addressInfo.Id);

                if (current == null)
                {
                    current = addressInfo;
                    try
                    {
                        _btcContext.Addresses.Add(current);
                        await _btcContext.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        _btcContext.Entry(current).State = EntityState.Detached;
                        updateFailed = true;
                    }
                }
                else
                {
                    current.Amount = addressInfo.Amount;
                    _btcContext.Addresses.Update(current);
                    await _btcContext.SaveChangesAsync();
                }                
            } while (updateFailed);
        }
    }
}