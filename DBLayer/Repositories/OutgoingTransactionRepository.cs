﻿using System.Linq;
using DBLayer.Contexts;
using DBLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DBLayer.Repositories
{
    public class OutgoingTransactionRepository
    {
        private readonly BtcContext _context;

        public OutgoingTransactionRepository(BtcContext context)
        {
            _context = context;
        }

        public void Insert(BtcOutgoingTransaction newTransaction)
        {
            bool ok;

            do
            {
                using (var ts = _context.Database.BeginTransaction())
                {
                    var txis = newTransaction.Id;

                    var current = _context.OutgoingTransactions.FirstOrDefault(t => t.Id == txis);

                    if (current != null)
                    {
                        if (current.Confirmations < 6 && newTransaction.Confirmations > current.Confirmations)
                        {
                            // update:
                            try
                            {
                                current.Confirmations = newTransaction.Confirmations;
                                _context.Update(current);
                                _context.SaveChanges();
                                ts.Commit();
                                ok = true;
                            }
                            catch (DbUpdateException)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = true;
                        }
                    }
                    else
                    {
                        try
                        {
                            // trying insert:
                            current = newTransaction;
                            _context.Add(current);
                            _context.SaveChanges();
                            ts.Commit();
                            ok = true;
                        }
                        catch (DbUpdateException)
                        {
                            _context.Entry(current).State = EntityState.Detached;
                            ok = false;                            
                        }
                    }
                }
            } while (!ok);
        }
    }
}