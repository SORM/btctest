﻿using DBLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DBLayer.Contexts
{
    public class BtcContext : DbContext
    {
        private readonly IConfiguration _configuration;

        public BtcContext(IConfiguration configuration)
        {
            _configuration = configuration;
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("Local"));
        }
        
        public DbSet<BtcIncomingTransaction> IncomingTransactions { get; set; }
        public DbSet<BtcOutgoingTransaction> OutgoingTransactions { get; set; }
        public DbSet<AddressInfo> Addresses { get; set; }
    }
}