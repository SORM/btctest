﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DBLayer.Models
{
    public class BtcTransaciton
    {
        public string Id { get; set; }
        public double Amount { get; set; }
        public DateTime Time { get; set; }
        public string Category { get; set; }
        public string Address { get; set; }
        public int Confirmations { get; set; }
        public bool Viewed { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}