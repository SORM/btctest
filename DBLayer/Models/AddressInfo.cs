﻿namespace DBLayer.Models
{
    public class AddressInfo
    {
        public string Id { get; set; }
        public double Amount { get; set; }
    }
}