﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DBLayer.Migrations
{
    public partial class InitDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncomingTransactions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    Category = table.Column<string>(nullable: true),
                    Confirmations = table.Column<int>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncomingTransactions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutgoingTransactions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    Category = table.Column<string>(nullable: true),
                    Confirmations = table.Column<int>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutgoingTransactions", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncomingTransactions");

            migrationBuilder.DropTable(
                name: "OutgoingTransactions");
        }
    }
}
