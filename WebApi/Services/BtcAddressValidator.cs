﻿using System.Linq;

namespace WebApi.Services
{
    internal class BtcAddressValidator
    {                
        public static bool Validate(string address)
        {

            if (address.Length < 26 || address.Length > 40)
                return false;

            const string alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
            if (!address.All(c => alphabet.Contains(c)))
                return false;

            switch (address[0])
            {
                case '1':
                case '2':
                case '3':
                    return true;
                default:
                    return false;            
            }            
        }
    }
}