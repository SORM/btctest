﻿using System.Net;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public interface ITransferService
    {
        Task<HttpStatusCode> SendAsync(string addressTo, double amount);
    }
}