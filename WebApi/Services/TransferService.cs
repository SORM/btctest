﻿using System;
using System.Net;
using System.Threading.Tasks;
using BtcRpcClient;

namespace WebApi.Services
{
    public class TransferService : ITransferService
    {
        private readonly IBtcRpcClient _btcRpcClient;
        
        public TransferService(IBtcRpcClient btcRpcClient)
        {
            _btcRpcClient = btcRpcClient;
        }
        public Task<HttpStatusCode> SendAsync(string addressTo, double amount)
        {
            if (!BtcAddressValidator.Validate(addressTo))
                throw new ArgumentException(nameof(addressTo));

            return _btcRpcClient.SendAsync(addressTo, amount);
        }
    }
}