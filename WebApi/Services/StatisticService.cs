﻿using System.Linq;
using System.Threading.Tasks;
using BtcRpcClient;
using DBLayer.Contexts;
using Microsoft.EntityFrameworkCore;
using WebApi.Dtos;

namespace WebApi.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IBtcRpcClient _btcRpcClient;
        private readonly BtcContext _btcContext;

        public StatisticService(IBtcRpcClient btcRpcClient, BtcContext btcContext)
        {
            _btcRpcClient = btcRpcClient;
            _btcContext = btcContext;
        }

        public async Task<LastReceivedStatDto[]> GetLastReceivedAsync()
        {
            var context = _btcContext;

            var receivedInfo = await context.IncomingTransactions.Where(t => t.Viewed == false || t.Confirmations < 3).ToListAsync();

            foreach (var btcIncomingTransaction in receivedInfo.Where(t=>t.Viewed == false))
            {
                btcIncomingTransaction.Viewed = true;
                context.Update(btcIncomingTransaction);

                try
                {
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // transaction has been updated externally, it has new data, so we won't make it viewed...
                }
            }

            return receivedInfo.Select(ri => new LastReceivedStatDto
            {
                Amount = ri.Amount,
                DateTime = ri.Time,
                AddressReceived = ri.Address,
                Confirmation = ri.Confirmations,
            }).ToArray();
        }

        public async Task<StatTransactionDto> GetTransaction(string txid)
        {
            var tinfo = await _btcRpcClient.GetTransaction(txid);

            if (tinfo == null) return null;

            return new StatTransactionDto
            {
                Amount = tinfo.Amount,
                Confirmations = tinfo.Confirmations,
                Details = tinfo.Details,
                Timereceived = tinfo.Timereceived,
                TxId = tinfo.TxId,
            };
        }
    }
}