﻿using System.Threading.Tasks;
using WebApi.Dtos;

namespace WebApi.Services
{
    public interface IStatisticService 
    {
        Task<LastReceivedStatDto[]> GetLastReceivedAsync();
        Task<StatTransactionDto> GetTransaction(string txid);
    }
}