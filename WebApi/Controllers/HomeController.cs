﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;

namespace WebApi.Controllers
{
    public class MainController : Controller
    {
        private readonly IStatisticService _statisticService;
        private readonly ITransferService _transferService;

        public MainController(IStatisticService statisticService, ITransferService transferService)
        {
            _statisticService = statisticService;
            _transferService = transferService;
        }
        
        [Route("GetLast")]
        public async Task<IActionResult> GetLast()
        {
            var data = await _statisticService.GetLastReceivedAsync();
            return Json(data);
        }

        [Route("SendBtc/{address}/{amount}")]
        public async Task<IActionResult> SendBtc(string address, double amount)
        {
            try
            {
                await _transferService.SendAsync(address, amount);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
            ;
            return Ok();
        }
    }
}