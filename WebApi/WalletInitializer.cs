﻿using System.Linq;
using System.Threading.Tasks;
using BtcRpcClient;
using DBLayer.Models;
using DBLayer.Repositories;

namespace WebApi
{
    public class WalletInitializer
    {
        private readonly IWalletRepository _repository;
        private readonly IBtcRpcClient _btcRpcClient;

        public WalletInitializer(IWalletRepository repository, IBtcRpcClient btcRpcClient)
        {
            _repository = repository;
            _btcRpcClient = btcRpcClient;
        }

        public async Task Initialize()
        {
            var addresses = await _btcRpcClient.GetListReceivedByAddress();
            if (addresses == null) return;

            var toUpdate = addresses.Select(a => new AddressInfo {Amount = a.Amount, Id = a.Address}).ToArray();
            await _repository.UpdateAddressesAsync(toUpdate);
        }
    }
}