﻿using BtcRpcClient;
using DBLayer.Contexts;
using DBLayer.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Services;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IBtcRpcClient, Client>(provider => new Client(Configuration["login"],
                Configuration["password"], Configuration["url"], Configuration["port"]));
            services.AddTransient<ITransferService, TransferService>();
            services.AddTransient<IStatisticService, StatisticService>();
            services.AddTransient<IWalletRepository, WalletRepository>();
            services.AddTransient<WalletInitializer>();
            services.AddDbContext<BtcContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Local")));

            services.AddMvc();


            var sb = services.BuildServiceProvider();
            var wi = sb.GetService<WalletInitializer>();
            wi.Initialize().Wait();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
