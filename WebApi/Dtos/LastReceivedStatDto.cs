﻿using System;

namespace WebApi.Dtos
{
    public class LastReceivedStatDto
    {
        public DateTime DateTime { get; set; }
        public string AddressReceived { get; set; }
        public double Amount { get; set; }
        public int Confirmation { get; set; }

    }
}