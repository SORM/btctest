﻿using System;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace BtcRpcClient.Tests
{
    public class ClietnApiTest
    {
        private Client _client;

        public ClietnApiTest()
        {
            var cfg = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _client = new Client(cfg["login"], cfg["password"], cfg["url"], cfg["port"]);
        }

        [Fact]
        public async void GetListReceivedTest()
        {
            var ret = await _client.GetListReceivedByAddress();
            Assert.NotNull(ret);
        }
    }
}