using BtcRpcClient.RequestBuilders;
using Newtonsoft.Json;
using Xunit;

namespace BtcRpcClient.Tests
{
    public class RequestTest
    {
        [Fact]
        public void GetBalance()
        {
            var rb = new GetBalanceRequest();
            var res = rb.Build();
            Assert.Equal("{\"jsonrpc\":\"1.0\",\"id\":\"1\",\"method\":\"getbalance\"}", res);
        }

        [Fact]
        public void SendBtc()
        {
            var rb = new SendBtcRequest();
            const string address = "qwe";
            var res = rb.Build(address, 0.1);
            Assert.Equal("{\"jsonrpc\":\"1.0\",\"id\":\"sendbts_qwe_0,1\",\"method\":\"sendtoaddress\",\"params\":[\"qwe\",0.1]}", res);
        }


        [Fact]
        public void GetTransactionError()
        {
            var rb = new GetTransactionRequest();
            const string txid = "qwe";
            var res = rb.Build(txid);
            Assert.Equal("{\"jsonrpc\":\"1.0\",\"id\":\"gettxinfo_qwe\",\"method\":\"gettransaction\",\"params\":[\"qwe\"]}", res);
        }

        [Fact]
        public void GetListReceivedByAddress()
        {
            var rb = new GetListReceivedByAddress();
            var res = rb.Build();
            Assert.NotNull(res);
            var o = JsonConvert.DeserializeObject(res);
            Assert.NotNull(o);
        }
    }
}
