﻿using System.Linq;
using DBLayer.Contexts;
using WebApi.Services;
using Xunit;

namespace WebApi.Tests
{
    public class StatisticServiceTests : TestsBase
    {
        private BtcContext Context => new BtcContext(Cfg);

        [Fact]
        public async void GetLast_correct_values_resurn_200()
        {
            var service = new StatisticService(Client, Context);
            var res = await service.GetLastReceivedAsync();
            Assert.NotNull(res);
        }


        [Fact]
        public async void GetTransactionWorks()
        {
            var service = new StatisticService(Client, Context);
            var res = await service.GetTransaction("30a6e2d7b286f5bed7eea0cc989b1d68f3ede572eb1a4bf8c0059fa91f0a7f95");
            Assert.NotNull(res);
            Assert.True(res.Amount > 0);
            Assert.NotNull(res.Details);
            Assert.True(res.Details.Length > 0);
            Assert.NotNull(res.Details.First().Address);
            Assert.NotNull(res.Details.First().Category);
            Assert.True(res.Details.First().Amount > 0);
        }
    }
}