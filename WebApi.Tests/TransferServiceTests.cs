using System.Net;
using WebApi.Services;
using Xunit;

namespace WebApi.Tests
{
    public class TransferServiceTests : TestsBase
    {
        [Fact]
        public async void Insufficient_funds_500()
        {
            var service = new TransferService(Client);
            var res = await service.SendAsync("2N8hwP1WmJrFF5QWABn38y63uYLhnJYJYTF", double.MaxValue);
            Assert.Equal(HttpStatusCode.InternalServerError, res);
        }
    }
}
