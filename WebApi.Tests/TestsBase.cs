﻿using BtcRpcClient;
using Microsoft.Extensions.Configuration;

namespace WebApi.Tests
{
    public class TestsBase
    {
        protected readonly Client Client;
        protected readonly IConfiguration Cfg;

        protected TestsBase()
        {
            Cfg = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            Client = new Client(Cfg["login"], Cfg["password"], Cfg["url"], Cfg["port"]);
        }
    }
}