﻿namespace BtcRpcClient.Dtos
{
    public class AddressInfoDto
    {
        public string Address { get; set; }
        public double Amount { get; set; }
    }
}