﻿namespace BtcRpcClient.Dtos
{
    public class DetailInfoDto
    {
        public string Address { get; set; }
        public string Category { get; set; }
        public double Amount { get; set; }

        public override string ToString()
        {
            return $"[amount = {Amount} address = {Address} cat = {Category}]";
        }
    }
}