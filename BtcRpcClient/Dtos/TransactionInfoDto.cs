﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BtcRpcClient.Dtos
{
    public class TransactionInfoDto
    {
        public double Amount { get; set; }
        public int Confirmations { get; set; }
        public string TxId { get; set; }
        [JsonConverter(typeof(MyDateTimeConverter))]
        public DateTime Timereceived { get; set; }
        public DetailInfoDto[] Details { get; set; }

        public override string ToString()
        {
            return $"amount = {Amount} confirmations = {Confirmations}, details = {(Details.Length > 0 ? Details.First() : null)}, txid = {TxId}";
        }
    }
}