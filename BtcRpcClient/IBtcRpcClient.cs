﻿using System.Net;
using System.Threading.Tasks;
using BtcRpcClient.Dtos;

namespace BtcRpcClient
{
    public interface IBtcRpcClient
    {
        Task<HttpStatusCode> SendAsync(string addressTo, double amount);
        Task<TransactionInfoDto> GetTransaction(string txid);
        Task<AddressInfoDto[]> GetListReceivedByAddress();
    }
}