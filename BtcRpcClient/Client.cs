﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BtcRpcClient.Dtos;
using BtcRpcClient.RequestBuilders;
using Newtonsoft.Json;

namespace BtcRpcClient
{
    public class Client : IBtcRpcClient
    {
        private readonly string _login;
        private readonly string _password;
        private readonly string _url;
        private readonly string _port;
        private readonly SendBtcRequest _sendBtcRequest = new SendBtcRequest();
        private readonly GetTransactionRequest _getTransactionRequest = new GetTransactionRequest();
        private readonly GetListReceivedByAddress _getListReceived = new GetListReceivedByAddress();

        public Client(string login, string password, string url, string port)
        {
            _port = port;
            _url = url;
            _login = login;
            _password = password;
        }        

        public async Task<HttpStatusCode> SendAsync(string addressTo, double amount)
        {
            var request = WebRequest.CreateHttp($"{_url}:{_port}");
            request.ContentType = "application/json";
            request.Credentials = new NetworkCredential(_login, _password);
            request.Method = "POST";

            var json = _sendBtcRequest.Build(addressTo, amount);
            var data = Encoding.UTF8.GetBytes(json);
            request.ContentLength = data.Length;
            using (var rs = request.GetRequestStream())
            {
                await rs.WriteAsync(data, 0, data.Length);
            }

            try
            {
                using (var response =(HttpWebResponse) await request.GetResponseAsync())
                {
                    return response.StatusCode == HttpStatusCode.OK ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
                }
            }
            catch (WebException)
            {
                return HttpStatusCode.InternalServerError;
            }
        }

        public async Task<TransactionInfoDto> GetTransaction(string txid)
        {
            var request = WebRequest.CreateHttp($"{_url}:{_port}");
            request.ContentType = "application/json";
            request.Credentials = new NetworkCredential(_login, _password);
            request.Method = "POST";

            var json = _getTransactionRequest.Build(txid);
            var data = Encoding.UTF8.GetBytes(json);
            request.ContentLength = data.Length;
            using (var rs = request.GetRequestStream())
            {
                await rs.WriteAsync(data, 0, data.Length);
            }

            try
            {
                using (var response = (HttpWebResponse)await request.GetResponseAsync())
                {
                    using (var rs = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(rs))
                        {
                            var body = await sr.ReadToEndAsync();
                            var ret = JsonConvert.DeserializeObject<GetTransactionResultDto>(body, new MyDateTimeConverter());
                            return ret.Result;
                        }
                    }
                }
            }
            catch (WebException e)
            {
                return null;
            }
        }

        public async Task<AddressInfoDto[]> GetListReceivedByAddress()
        {
            var request = WebRequest.CreateHttp($"{_url}:{_port}");
            request.ContentType = "application/json";
            request.Credentials = new NetworkCredential(_login, _password);
            request.Method = "POST";

            var json = _getListReceived.Build();
            var data = Encoding.UTF8.GetBytes(json);
            request.ContentLength = data.Length;
            using (var rs = request.GetRequestStream())
            {
                await rs.WriteAsync(data, 0, data.Length);
            }

            try
            {
                using (var response = (HttpWebResponse)await request.GetResponseAsync())
                {
                    using (var rs = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(rs))
                        {
                            var body = await sr.ReadToEndAsync();
                            var ret = JsonConvert.DeserializeObject<GetListReceivedResultDto>(body);
                            return ret.Result;
                        }
                    }
                }
            }
            catch (WebException e)
            {
                return null;
            }
        }
    }

    class GetTransactionResultDto
    {
        public TransactionInfoDto Result { get; set; }
        public string Id { get; set; }
    }

    class GetListReceivedResultDto
    {
        public AddressInfoDto[] Result { get; set; }        
    }

    public class MyDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var t = (long) reader.Value;
            return  new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(t);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}