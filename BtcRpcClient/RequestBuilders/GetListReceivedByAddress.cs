﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BtcRpcClient.RequestBuilders
{
    public class GetListReceivedByAddress
    {
        public string Build()
        {
            var joe = new JObject
            {
                new JProperty("jsonrpc", "1.0"),
                new JProperty("id", $"listreceivedbyaddress_{Guid.NewGuid()}"),
                new JProperty("method", "listreceivedbyaddress"),
                new JProperty("params", 0, true),
            };

            return JsonConvert.SerializeObject(joe);

        }
    }
}