﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BtcRpcClient.RequestBuilders
{
    public class GetTransactionRequest
    {
        public string Build(string txid)
        {
            var joe = new JObject
            {
                new JProperty("jsonrpc", "1.0"),
                new JProperty("id", $"gettxinfo_{txid}"),
                new JProperty("method", "gettransaction"),
                new JProperty("params", new object[]{txid})
            };

            return JsonConvert.SerializeObject(joe);

        }

    }
}