﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BtcRpcClient.RequestBuilders
{
    public class GetBalanceRequest
    {
        public string Build()
        {
            var joe = new JObject
            {
                new JProperty("jsonrpc", "1.0"),
                new JProperty("id", "1"),
                new JProperty("method", "getbalance"),               
            };

            return JsonConvert.SerializeObject(joe);
        }
    }
}