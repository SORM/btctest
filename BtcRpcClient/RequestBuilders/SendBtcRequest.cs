﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BtcRpcClient.RequestBuilders
{
    public class SendBtcRequest
    {
        public string Build(string address, double amount)
        {
            var joe = new JObject
            {
                new JProperty("jsonrpc", "1.0"),
                new JProperty("id", $"sendbts_{address}_{amount}"),
                new JProperty("method", "sendtoaddress"),
                new JProperty("params", address, amount)
            };

            return JsonConvert.SerializeObject(joe);

        }
    }
}