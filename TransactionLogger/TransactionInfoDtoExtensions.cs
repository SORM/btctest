﻿using BtcRpcClient.Dtos;
using DBLayer.Models;

namespace TransactionLogger
{
    internal static class TransactionInfoDtoExtensions
    {
        public static BtcIncomingTransaction ToIncomingTransaction(this TransactionInfoDto infoDto)
        {
            return new BtcIncomingTransaction
            {
                Id = infoDto.TxId,
                Amount = infoDto.Amount,
                Confirmations = infoDto.Confirmations,
                Time = infoDto.Timereceived,
                Category = infoDto.Details[0].Category,
                Address = infoDto.Details[0].Address,
                Viewed = false,
            };
        }

        public static BtcOutgoingTransaction ToOutgoingTransaction(this TransactionInfoDto infoDto)
        {
            return new BtcOutgoingTransaction
            {
                Id = infoDto.TxId,
                Amount = infoDto.Amount,
                Confirmations = infoDto.Confirmations,
                Time = infoDto.Timereceived,
                Category = infoDto.Details[0].Category,
                Address = infoDto.Details[0].Address,
                Viewed = false,
            };
        }
    }
}