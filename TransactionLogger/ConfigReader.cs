﻿using Microsoft.Extensions.Configuration;

namespace TransactionLogger
{
    internal class ConfigReader
    {
        public string Login { get; }
        public string Password { get; }
        public string Port { get; }
        public string Url { get; }

        public ConfigReader(IConfiguration configurationRoot)
        {
            Login = configurationRoot["login"];
            Password = configurationRoot["password"];
            Url = configurationRoot["url"];
            Port = configurationRoot["port"];
        }

        public override string ToString()
        {
            return $"{Url}:{Port} with {Login}:{Password}";
        }
    }
}