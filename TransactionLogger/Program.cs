﻿using System;
using System.Data.Common;
using System.Linq;
using BtcRpcClient;
using BtcRpcClient.Dtos;
using DBLayer.Contexts;
using DBLayer.Models;
using DBLayer.Repositories;
using Microsoft.Extensions.Configuration;
using NLog;

namespace TransactionLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1) return;

            var logger = LogManager.GetLogger("main");

            var txid = args.First();

            var configurationRoot = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var cfg = new ConfigReader(configurationRoot);

            logger.Log(LogLevel.Info, cfg);

            var client = new Client(cfg.Login, cfg.Password, cfg.Url, cfg.Port);
            var info = client.GetTransaction(txid);
            info.Wait();

            var newTransaction = info.Result;
            if (newTransaction == null)
            {
                logger.Error("Received transaction from demon is NULL, is demon working now?");
                return;
            }

            logger.Log(LogLevel.Info, $"new incoming transaction: {newTransaction}");

            try
            {
                InsertTransaction(configurationRoot, newTransaction, logger);
                UpdateWallet(client, configurationRoot, logger);
            }
            catch (ApplicationException e)
            {
                Console.WriteLine(e);
                logger.Error(e);
            }
        }

        private static void UpdateWallet(IBtcRpcClient client, IConfiguration configuration, ILogger logger)
        {
            try
            {
                var getTask = client.GetListReceivedByAddress();
                getTask.Wait();

                if (getTask.Result == null) return;

                var addresses = getTask.Result.Select(a => new AddressInfo { Amount = a.Amount, Id = a.Address }).ToArray();

                using (var context = new BtcContext(configuration))
                {
                    var repo = new WalletRepository(context);
                    repo.UpdateAddressesAsync(addresses).Wait();
                }
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                logger.Error(e);
                throw new ApplicationException($"wallet update error: {e.Message}", e);
            }
        }

        private static void InsertTransaction(IConfiguration configuration, TransactionInfoDto newTransaction, ILogger logger)
        {
            try
            {
                using (var context = new BtcContext(configuration))
                {
                    var isOutgoingTransaction =
                        string.Equals(newTransaction.Details[0].Category, "send", StringComparison.Ordinal);

                    if (isOutgoingTransaction)
                    {
                        var repo = new OutgoingTransactionRepository(context);
                        repo.Insert(newTransaction.ToOutgoingTransaction());
                    }
                    else
                    {
                        var repo = new IncomingTransactionRepository(context);
                        repo.Insert(newTransaction.ToIncomingTransaction());
                    }
                }
            }
            catch (DbException e)
            {
                Console.WriteLine(e);
                logger.Error(e);
                throw new ApplicationException($"insert transaction error: {e.Message}", e);
            }
        }
    }
}
